<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js" crossorigin="anonymous"></script>

    <script type="text/javascript" language="javascript" >
      $(document).ready(function(){
        $('.input-daterange').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
        });

        $('table').DataTable({
          "searching": false
        });

        $('#search').click(function(){
          var start_date = $('#start_date').val();
          var end_date = $('#end_date').val();
          if(start_date != '' && end_date !='')
          {
            $('table').DataTable().destroy();
            $('table').DataTable({
              "processing" : true,
              "order" : [],
              "ajax" : {
                url:"<?=site_url("Filter/search")?>",
                type:"POST",
                data:{
                  start_date:start_date, end_date: end_date
                }
              },
              "columns": [
                  { "data": "id" },
                  { "data": "nama_buku" },
                  { "data": "start_date" },
                  { "data": "end_date" }
              ]
            });
          }
          else
          {
            alert("Both Date is Required");
          }
        }); 
      });
    </script>
  </body>
</html>