<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Buku extends CI_Model {
    public function getAllData($table){
        return $this->db->get($table)->result();
    }

    public function filter($table, $start_date, $end_date){
        $this->db->where('start_date >=',$start_date); 
        $this->db->where('end_date <=',$end_date);
        return $this->db->get($table)->result();
    }
}
